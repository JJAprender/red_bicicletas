var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");

describe("Testing bicicletas", function () {
    beforeEach( function(done) {
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, { useNewUrlParser: true , useUnifiedTopology: true });
        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "connection error"));
        db.once("open", function () {
            console.log("we are connect to test database");
           
            done();
        });
    });
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
           
        });
    });
    describe("Bicicleta.createInstance", () => {
        it("Crea una instancia de Bicicleta", (done) => {
            var bici = Bicicleta.createInstance(1, "rojo", "urbana", [-34.1, -35.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("rojo");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.1);
            expect(bici.ubicacion[1]).toEqual(-35.1);
            done();
        });
    });


    describe("Bicicleta.allBicis", () => {
        it("Comienze vacia", (done) => {
            Bicicleta.allBicis( function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });
    describe('Bicicleta.add', () => {
        it('Agregamos una', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "montaña"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    if(err) console.log(err);

                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
    
                    done();
                });            
            });
           
        });
    });
    describe('Bicicleta.findById', () => {
        it('Encontramos una bicicleta con code 1', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                  
                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "montaña"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    var aBici2 = new Bicicleta({code: 2, color: "amarillo", modelo: "montaña"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1,function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();


                        });


                    });    
                });          
            });
           
        });
    });
    describe('Bicicleta.removeByCode', () => {
        it('eliminamos una bicicleta con code 1', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                  
                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "montaña"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);
                    var aBici2 = new Bicicleta({code: 2, color: "amarillo", modelo: "montaña"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.removedByCode(1,function(error, targetBici){
                            expect(targetBici.code).toBe(undefined);
                            done();


                        });


                    });    
                });          
            });
           
        });
    });
});

//  beforeEach( () => {Bicicleta.allBicis= [];});

// describe('Bicicleta.allBicis', () => {
//     it('Comienze vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//     });
// });

// describe('Bicicleta.add', () => {
//     it('Agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1,'rojo', 'urbana',[-25.3288524,-57.5970915]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// });
// describe('Bicicleta.findBy', () => {
//     it('Debe devolver la bici con el id 1 ', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var aBici = new Bicicleta(1,"verde","urbana");
//         var aBici2 = new Bicicleta(2,"rojo","urbana");
//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);
//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(aBici.color);
//         expect(targetBici.modelo).toBe(aBici.modelo);
//     });
// });
// describe('Bicicleta.removeById', () => {
//     it('Eliminamos un Id', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1,'rojo', 'urbana',[-25.3288524,-57.5970915]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//         Bicicleta.removeById(1);
//         expect(Bicicleta.allBicis.length).toBe(0);

//     });
// });
