var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "http://localhost:3000/api/bicicletas";
describe("Testing bicicletas", function () {
    beforeEach( function(done) {
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.createConnection(mongoDB, { useNewUrlParser: true , useUnifiedTopology: true });
        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "connection error"));
        db.once("open", function () {
            console.log("we are connect to test database");
           
            done();
        });
    });
    afterEach(function (done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
           
        });
    });
    describe("GET/Bicicleta /",() =>{
        it('Status 200', (done)=>{
            request.get(base_url,function(error, response,body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.Bicicletas.length).toBe(0);
                done();
            });
        });
    });
    describe("POST/ Bicicleta /create ",()=> {
        it("Status 200 ",(done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"code":10,"color": "rojo","modelo":"urbana","lat":-34,"lng":20}'
            request.post({
                headers:headers,
                url: base_url + '/create',
                body: aBici
            },function(error,response,body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(20);
                done();
            });
        });
    });
});
// describe("Bicicleta API", () => {
//     describe("GET BICICLETAS /", () => {
//         it("Status 200", () => {
//             expect(Bicicleta.allBicis.length).toBe(0);

//             var a = new Bicicleta(1, "rojo", "urbana", [-25.3288524, -57.5970915]);
//             Bicicleta.add(a);

//             request.get("http://localhost:3000/api/bicicletas", function (
//                 error,
//                 response,
//                 body
//             ) {
//                 expect(response.statusCode).toBe(200);
//             });
//         });
//     });
//     describe("POST BICICLETAS /create", () => {
//         it("Status 200", (done) => {
//             var headers = { "content-type": "application/json" };
//             var aBici =
//                 '{"id": 10,"color": "rojo", "modelo":"urbana","lat":-25.3288524,"lng":-57.5970915}';
//             request.post(
//                 {
//                     headers: headers,
//                     url: "http://localhost:3000/api/bicicletas/create",
//                     body: aBici,
//                 },
//                 function (error, response, body) {
//                     expect(response.statusCode).toBe(200);
//                     expect(Bicicleta.findById(10).color).toBe("rojo");
//                     done();
//                 }
//             );
//         });
//     });
//     describe("POST BICICLETAS /Delete", () => {
//         it("Status 204", (done) => {
//             var a = new Bicicleta(10, "rojo", "urbana", [-25.3288524, -57.5970915]);
//             Bicicleta.add(a);
//             request.delete("http://localhost:3000/api/bicicletas/delete", function (
//                 error,
//                 response,
//                 body
//             ) {
//                 expect(Bicicleta.findById(10).color).toBe("rojo");
//                 expect(Bicicleta.removeById(10));
//                 expect(response.statusCode).toBe(204);
//                 done();
//             });
//         });
//     });
//     describe("POST BICICLETAS /update", () => {
//         it("Status 200", (done) => {
//             var headers = { "content-type": "application/json" };
//             var a = new Bicicleta(10, "rojo", "urbana", [-25.3288524, -57.5970915]);
//             Bicicleta.add(a);
//             bici = '{"id": 10,"color": "lima", "modelo":"urbana","lat":-25.3288524,"lng":-57.5970915}';
//             request.post(
//                 {
//                     headers: headers,
//                     url: "http://localhost:3000/api/bicicletas/update",
//                     body: bici,
//                 },
//                 function (error, response, body) {
//                     expect(response.statusCode).toBe(200);
//                     expect(Bicicleta.allBicis.length).toBe(1);
//                     expect(Bicicleta.findById(10).color).toBe("lima");
//                     done();
//                 }
//             );
//         });
//     });
    
// });
